const int buzzer = 10;
const int PIR = 6;

void setup() {
  pinMode(PIR, INPUT);
}

void loop() {
  if(digitalRead(PIR)==HIGH) {
    tone(buzzer, 30000); // Emite una señal acustica no audible por personas
    // pero molesta para perros cada vez que hay movimiento
  } else {
    noTone(buzzer);
  }
}
