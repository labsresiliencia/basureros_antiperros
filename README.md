# Basureros anti-perros

El Basurero Anti-Perros es un dispositivo electrónico que
detecta movimiento frente al basurero y emite una señal
sonora que no es audible por las personas pero resulta
molesta para los perros. El zumbido solo se activa cuando
se detecta movimiento frente al basurero.

Este repositorio contiene el código fuente e 
instrucciones de armado del proyecto denominado 
"Basureros anti-perros" desarrollado como parte de los 
Laboratorios de Resiliencia Comunitarios.

Prototipo elaborado por: Mario Gómez @ Hackerspace San Salvador

## Lista de Materiales
Para construir este proyecto necesitas los siguientes materiales:

| Nombre                          | Cantidad |
|---------------------------------|----------|
| Arduino UNO R3                  |        1 |
| Porta-baterías                  |        2 |
| Cable Jumper                    |        5 |
| Sensor tipo PIR                 |        1 |
| Buzzer                          |        1 |
| Breadboard                      |        1 |

## Diagrama de Conexión
![](./Docu/Esquematico_bb.png)

## Guía de Conexion en Breadboard
Las conexiónes que comienzan con **BB** se conectan en 
la Breadboard. Las que comienzan con **AR** se conectan 
al Arduino. Las que comienzan con **PB** se conectan al
porta-baterías.

| Componente                         |        |         |        |
|------------------------------------|--------|---------|--------|
| Sensor PIR                         | BB-E55 | BB-E56  | BB-E57 |
| Portabatería 1                     | BB-B25 | BB-V+   |        |
| Portabatería 2                     | BB-V-  | BB-A25  |        |
| Jumper 1                           | BB-V-  | AR-GND  |        |
| Jumper 2                           | BB-V+  | AR-VIN  |        |
| Jumper 3                           | BB-F55 | AR-5V   |        |
| Jumper 4                           | BB-F57 | AR-6    |        |
| Jumper 5                           | BB-F56 | BB-V-   |        |
| Buzzer                             | BB-V-  | AR-10   |        |

## Información para Colaborar con este Proyecto
Para colaborar con este proyecto solicita acceso al canal
oficial de Slack en [https://labsresiliencia.slack.com](https://labsresiliencia.slack.com) 
y únete al canal ***#propuesta_b4***.

